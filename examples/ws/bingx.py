import asyncio

from exchange_connections.exchange.bingx.ws import BingxWS

symbol = "BTC-USDT"

ws = BingxWS(symbol=symbol)


@ws.receive("markPrice")
async def handler(data):
    print(data)


asyncio.run(ws.run())
