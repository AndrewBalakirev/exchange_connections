import asyncio

from exchange_connections.exchange.binance.ws import BinanceWS

symbol = "BTCUSDT"

ws = BinanceWS(symbol=symbol)


@ws.receive("markPrice@1s")
async def handler(data):
    print(data)


asyncio.run(ws.run())
