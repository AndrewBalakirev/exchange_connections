import asyncio

from exchange_connections.exchange.commex.ws import CommexWS

symbol = "BTCUSDT"

ws = CommexWS(symbol=symbol)


@ws.receive("markPrice@1s")
async def handler(data):
    print(data)


asyncio.run(ws.run())
