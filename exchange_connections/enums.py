from enum import Enum


class Side(str, Enum):
    LONG = "LONG"
    SHORT = "SHORT"


class TransactionType(str, Enum):
    BUY = "BUY"
    SELL = "SELL"
