import json
import random
from dataclasses import dataclass, field

from exchange_connections.exchange.base import ExchangeWSBase, ExchangeWSDispatcher


class BinanceWSDispatcher(ExchangeWSDispatcher):
    def __init__(self, symbol: str):
        super().__init__(symbol)
        self.stream_type_key = "stream"

    async def on_add_handler(self, stream_type: str):
        CHANNEL = {
            "method": "SUBSCRIBE",
            "params": [stream_type],
            "id": random.randint(1, 9999),
        }
        subStr = json.dumps(CHANNEL)
        await self.ws.send(subStr)

    async def on_message(self, message: str):
        if (
            message == "Ping"
        ):  # this is very important , if you receive 'Ping' you need to send 'Pong'
            await self.ws.send("Pong")
            return

        try:
            data = json.loads(message)
            await self.on_data(data)
        except Exception as e:
            print((e, message))


@dataclass
class BinanceWS(ExchangeWSBase):
    ws_url: str = "wss://fstream.binancefuture.com/stream"
    dispatcher_class: type[ExchangeWSDispatcher] = BinanceWSDispatcher

    def __post_init__(self):
        super().__post_init__()
        self.ws_url += "?streams="
        self.symbol = self.symbol.lower()

    def add_handler(self, data_type: str, callback):
        if self.handlers:
            self.ws_url += "/"

        self.ws_url += data_type
        super().add_handler(data_type, callback)
