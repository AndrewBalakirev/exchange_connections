import gzip
import io
import json
import uuid
from dataclasses import dataclass, field

from exchange_connections.exchange.base import ExchangeWSBase, ExchangeWSDispatcher


class BingxWSDispatcher(ExchangeWSDispatcher):
    async def on_add_handler(self, data_type: str):
        CHANNEL = {"id": str(uuid.uuid4()), "reqType": "sub", "dataType": data_type}
        subStr = json.dumps(CHANNEL)
        await self.ws.send(subStr)

    async def on_message(self, message: bytes):
        compressed_data = gzip.GzipFile(fileobj=io.BytesIO(message), mode="rb")
        decompressed_data = compressed_data.read()
        utf8_data = decompressed_data.decode("utf-8")
        if utf8_data == "Ping":  # this is very important , if you receive 'Ping' you need to send 'Pong'
            await self.ws.send("Pong")
            return

        try:
            data = json.loads(utf8_data)
            await self.on_data(data)
        except Exception as e:
            print((e, utf8_data))


@dataclass
class BingxWS(ExchangeWSBase):
    ws_url: str = field(init=False)
    dispatcher_class: type[ExchangeWSDispatcher] = BingxWSDispatcher

    def __post_init__(self):
        super().__post_init__()
        self.ws_url = "wss://open-api-swap.bingx.com/swap-market"
        self.symbol = self.symbol.upper()
