from dataclasses import dataclass
import hmac
import time
import urllib.parse
from enum import Enum
from hashlib import sha256

from exchange_connections.exchange.base import ExchangeAPIBase, Route


@dataclass
class BingXAPI(ExchangeAPIBase):
    api_key: str
    secret_key: str
    api_url: str = "https://open-api-vst.bingx.com"  # Ссылка на демо трейдинг

    class Routes(Route, Enum):
        BALANCE = Route("/openApi/swap/v2/user/balance", "GET")
        CREATE_ORDER = Route("/openApi/swap/v2/trade/order", "POST")
        SWITCH_LEVERAGE = Route("/openApi/swap/v2/trade/leverage", "POST")
        ALL_FEATURES = Route("/openApi/swap/v2/quote/contracts", "GET")
        PRICE = Route("/openApi/swap/v2/quote/price", "GET")
        KLINES = Route("/openApi/swap/v3/quote/klines", "GET")

    def get_sign(self, params):
        payload = urllib.parse.urlencode(params)
        return hmac.new(self.secret_key.encode("utf-8"), payload.encode("utf-8"), digestmod=sha256).hexdigest()

    def get_headers(self, headers):
        return {**headers, "X-BX-APIKEY": self.api_key}

    def get_params(self, params: dict):
        params = {**params, "timestamp": int(time.time() * 1000)}
        params["signature"] = self.get_sign(params)
        print(params)
        return params

    async def get_klines(self, symbol, interval, limit=500):
        return await self._http_request(
            self.Routes.KLINES,
            params={"symbol": symbol, "interval": interval, "limit": limit},
        )

    async def get_all_features(self):
        return await self._http_request(self.Routes.ALL_FEATURES)

    async def get_price(self, symbol):
        return await self._http_request(self.Routes.PRICE, params={"symbol": symbol})

    async def create_order(self, symbol, transaction_type, side, quantity):
        params = {
            "symbol": symbol,
            "side": transaction_type,
            "positionSide": side,
            "type": "MARKET",
            "quantity": quantity,
        }
        return await self._http_request(self.Routes.CREATE_ORDER, params=params)

    async def create_take_profit(self, symbol, transaction_type, side, quantity, price):
        params = {
            "symbol": symbol,
            "side": transaction_type,
            "positionSide": side,
            "type": "TAKE_PROFIT_MARKET",
            "quantity": quantity,
            "stopPrice": price,
        }
        return await self._http_request(self.Routes.CREATE_ORDER, params=params)

    async def create_stop_loss(self, symbol, transaction_type, side, quantity, price):
        params = {
            "symbol": symbol,
            "side": transaction_type,
            "positionSide": side,
            "type": "STOP_MARKET",
            "quantity": quantity,
            "stopPrice": price,
        }
        return await self._http_request(self.Routes.CREATE_ORDER, params=params)

    async def get_balance(self):
        return await self._http_request(self.Routes.BALANCE)

    async def switch_leverage(self, symbol, side, leverage):
        params = {
            "symbol": symbol,
            "side": side,
            "leverage": leverage,
        }
        return await self._http_request(self.Routes.SWITCH_LEVERAGE, params=params)
