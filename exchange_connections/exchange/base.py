import asyncio
from collections import defaultdict
from dataclasses import dataclass
from enum import Enum
from typing import NamedTuple

import aiohttp
from async_websocket_client.apps import AsyncWebSocketApp
from async_websocket_client.dispatchers import BaseDispatcher


class Route(NamedTuple):
    url: str
    method: str


class ExchangeAPIBase:
    api_url: str

    class Routes(Route, Enum):
        ...

    def get_headers(self, headers: dict) -> dict:
        return headers

    def get_params(self, params: dict) -> dict:
        return params

    async def get_klines(self, symbol, interval, limit=500):
        ...

    async def get_all_features(self):
        ...

    async def get_price(self, symbol):
        ...

    async def create_order(self, symbol, transaction_type, side, quantity):
        ...

    async def create_take_profit(self, symbol, transaction_type, side, quantity, price):
        ...

    async def create_stop_loss(self, symbol, transaction_type, side, quantity, price):
        ...

    async def get_balance(self):
        ...

    async def switch_leverage(self, symbol, side, leverage):
        ...

    async def _http_request(self, route: Route, **kwargs) -> dict:
        kwargs.setdefault("timeout", 20)
        kwargs["headers"] = self.get_headers(kwargs.get("headers", {}))
        kwargs["params"] = self.get_params(kwargs.get("params", {}))

        try:
            async with aiohttp.ClientSession() as session:
                data = await session.request(
                    method=route.method, url=self.api_url + route.url, **kwargs
                )
                return data and await data.json()
        except Exception as e:
            data = None
            print(e)


class ExchangeWSDispatcher(BaseDispatcher):
    def __init__(self, symbol: str):
        super().__init__()
        self.symbol = symbol
        self.connected = False
        self.handlers: dict = defaultdict(list)
        self.stream_type_key = "dataType"

    async def connect_handler(self, stream_type: str, callback):
        self.handlers[stream_type].append(callback)
        while True:
            await asyncio.sleep(1)

            if self.connected:
                await self.on_add_handler(stream_type)
                break

    async def on_add_handler(self, stream_type: str):
        ...

    async def on_connect(self):
        print("connected", self.symbol)
        self.connected = True

    async def on_data(self, data: dict):
        stream_type = data.get(self.stream_type_key)
        handlers = self.handlers.get(stream_type, [])
        for handler in handlers:
            await handler(data)


@dataclass
class ExchangeWSBase:
    symbol: str
    ws_url: str
    dispatcher_class: type[ExchangeWSDispatcher]

    def __post_init__(self):
        self.handlers = defaultdict(list)

    def add_handler(self, data_type: str, callback):
        stream_type = f"{self.symbol}@{data_type}"
        self.handlers[stream_type].append(callback)

    def receive(self, data_type: str):
        def wrapper(fn):
            self.add_handler(data_type, fn)
        return wrapper

    async def run(self):
        self.dispatcher = self.dispatcher_class(self.symbol)
        for data_type, callbacks in self.handlers.items():
            for callback in callbacks:
                asyncio.create_task(
                    self.dispatcher.connect_handler(data_type, callback)
                )
        self.ws = AsyncWebSocketApp(self.ws_url, self.dispatcher)
        await self.ws.run()

